﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

public class F4MenuItem
{
    [MenuItem("F4 Tools/Clear Data")]
    public static void ClearGameData()
    {
        string path = Application.persistentDataPath + "/GameData.json";
        PlayerPrefs.DeleteAll();
        if (File.Exists(path))
        {
            File.Delete(path);
        }
    }
}
