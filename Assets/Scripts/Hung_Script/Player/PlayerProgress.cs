﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerProgress : MonoBehaviour
{
    public bool touchingUI;

    public void OnStartMiniGame()
    {
        GameManager.Instance.PlayMiniGameEvent.Invoke(true);
    }

    public void OnStopMiniGame()
    {
        GameManager.Instance.PlayMiniGameEvent.Invoke(false);
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (IsPointerUIsObject())
            {
                //OnStartMiniGame();
                //lock input player
            }
            else
            {
                //unlock input player
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            touchingUI = false;
        }
    }

    private bool IsPointerUIsObject()
    {
        if (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.currentSelectedGameObject != null)
        {
            touchingUI = true;
            return true;
        }
        else
        {
            touchingUI = false;
            return false;
        }
        //PointerEventData eventData = new PointerEventData(EventSystem.current);
        //eventData.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        //List<RaycastResult> results = new List<RaycastResult>();
        //EventSystem.current.RaycastAll(eventData, results);
        //return results.FindAll(result => result.gameObject.layer == LayerMask.NameToLayer("UI")).Count > 0;
    }

}
