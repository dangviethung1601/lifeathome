﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIButton : Button
{

    protected override void Start()
    {
        base.Start();
        onClick.AddListener(() => PlaySFXClickButton());
    }

    private void PlaySFXClickButton()
    {
        var audioSource = UIManager.Instance.audioSource;
        if (audioSource.clip == null)
        {
            audioSource.volume = SoundManager.Instance.GlobalVolume;
            audioSource.clip = SoundManager.Instance.audioButtonClick;
        }
        SoundManager.Instance.PlaySFX(audioSource, audioSource.clip, SoundManager.Instance.GlobalVolume);
    }
}
