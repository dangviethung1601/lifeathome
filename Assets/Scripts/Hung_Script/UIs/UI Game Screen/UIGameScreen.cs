﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGameScreen : MonoBehaviour
{
    [Header("Player enery bar")]
    public ProgressBar playerEnergyBar;
    public float smoothLerp = 5f;

    private float Energy => GameManager.Instance.playerEnergy;

    private void Start()
    {
        playerEnergyBar.ValueProgress = Energy / 100f;
    }

    private void Update()
    {
        if (Energy != playerEnergyBar.ValueProgress)
        {
            playerEnergyBar.ValueProgress = Mathf.Lerp(playerEnergyBar.ValueProgress, Energy / 100f, Time.deltaTime * smoothLerp);
        }
    }
}
