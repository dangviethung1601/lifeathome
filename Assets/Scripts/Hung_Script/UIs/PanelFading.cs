﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PanelFading : MonoBehaviour
{
    [Header("Setting")]
    [SerializeField] float fadingTime;
    [SerializeField] Image thisImage;
    private CanvasGroup canvasGroup;

    private void Start()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = 0f;
    }

    /// <summary>
    /// Make sure button cannot click
    /// </summary>
    /// <param name="enableRaycastTarget"></param>
    private void SetRaycastTargetCheck(bool enableRaycastTarget)
    {
        thisImage.raycastTarget = enableRaycastTarget;
    }

    public void StartFadePanel(Action firstAction = null, Action actionOnComplete = null)
    {
        SetRaycastTargetCheck(true);
        LeanTween.alphaCanvas(canvasGroup, 1f, fadingTime).setOnComplete(() =>
        {
            LeanTween.delayedCall(0.2f, () =>
            {
                firstAction?.Invoke();
                LeanTween.alphaCanvas(canvasGroup, 0f, fadingTime).setOnComplete(() =>
                {
                    actionOnComplete?.Invoke();
                    SetRaycastTargetCheck(false);
                });
            });
        });
    }

    private IEnumerator IEStartFade(float fadingTime, float targetAlpha)
    {
        float smooth = 0f;
        Color imageColor = thisImage.color;
        Color targetColor = new Color
        {
            a = targetAlpha
        };
        while (smooth < fadingTime)
        {
            smooth += Time.deltaTime;
            imageColor = Color.Lerp(imageColor, targetColor, smooth / fadingTime);
            thisImage.color = imageColor;
            if (thisImage.color.a == targetAlpha)
            {
                yield break;
            }
            yield return null;
        }
    }
}
