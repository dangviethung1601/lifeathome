﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using System.Text;

public class SaveLoadGameData : Singleton<SaveLoadGameData>
{
    public GameData gameData;
    public AllRooms allRooms;

    private void Awake()
    {
        LoadGameData();
    }

    public void SaveGameData(GameData data)
    {
        string content = JsonUtility.ToJson(data);
        File.WriteAllText(Application.persistentDataPath + "/GameData.json", content);
    }

    public void LoadGameData()
    {
        if (!Directory.Exists(Application.persistentDataPath))
        {
            Directory.CreateDirectory(Application.persistentDataPath);
        }

        string path = Application.persistentDataPath + "/GameData.json";
        if (File.Exists(path))
        {
            string content = File.ReadAllText(path);
            gameData = JsonUtility.FromJson<GameData>(content);
        }
        else
        {
            RefreshGameData();
        }

        TextAsset itemConfig = Resources.Load<TextAsset>("Configs/RoomsConfig");
        allRooms = JsonUtility.FromJson<AllRooms>(itemConfig.text);
    }

    public void RefreshGameData()
    {
        gameData = new GameData
        {
            room = 1,
            gameProgress = 0f,
            itemIndex = 1,
            playerEnergy = 100f,
            isCompleteGame = false
        };
        File.WriteAllText(Application.persistentDataPath + "/GameData.json", JsonUtility.ToJson(gameData));
    }
}
