﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class GameData
{
    public int itemIndex;
    public int room;
    public float gameProgress;
    public float playerEnergy;
    public bool isCompleteGame;
}

[Serializable]
public class Item
{
    public int room;
    public int itemID;
    public string itemName;
    public int itemBonus;
}

[Serializable]
public class RoomData
{
    public int roomNumber;
    public float amountEnergyReduced;
    public float periodReducePlayerEnergy;
    public Item[] listItems;
}

[Serializable]
public class AllRooms
{
    public RoomData[] roomDatas;

    public RoomData GetCurrentRoomData(int roomIndex)
    {
        for (int i = 0; i < roomDatas.Length; i++)
        {
            if (roomDatas[i].roomNumber == roomIndex)
            {
                return roomDatas[i];
            }
        }
        return roomDatas[0];
    }
}