﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager>
{
    public const int totalRoom = 4;

    [Header("Camera")]
    public Camera mainCamera;

    [Header("Pause Game")]
    public bool isPlaying = true;

    [Header("Parameters")]
    public int currentRoom;
    public int currentItemIndex;
    public bool isCompleteGame;
    public float gameProgress;

    public delegate void FinishMiniGame();
    private event FinishMiniGame finishMiniGameEvent;

    public delegate void PlayMiniGame(bool startMiniGame);
    private event PlayMiniGame playMiniGameEvent;

    public PlayMiniGame PlayMiniGameEvent => playMiniGameEvent;
    public FinishMiniGame FinishGameEvent => finishMiniGameEvent;

    [Header("Player Energy")]
    public float playerEnergy;
    public float amountEnergyReduced;
    public float bonusEnergy;
    private float originEnergy;

    [Header("Game Play")]
    public Camera inHomeCamera;
    public Camera miniGameCamera;

    private void Start()
    {
        InitializeData();

    }


    private void InitializeData()
    {
        originEnergy = playerEnergy;
    }

    public void Win()
    {
        UIManager.Instance.panelWinGame.SetActive(true);
        LeanTween.delayedCall(1f, () =>
        {
            SceneManager.LoadScene("Hung_Scene");
        });
    }

    private void UpdateGameData()
    {
        GameData data = new GameData
        {
            room = currentRoom,
            gameProgress = gameProgress,
            playerEnergy = playerEnergy,
            itemIndex = currentItemIndex,
            isCompleteGame = isCompleteGame
        };
        SaveLoadGameData.Instance.SaveGameData(data);
    }

    //public void StartReducedPlayerEnergy(bool startMiniGame)
    //{
    //    if (startMiniGame)
    //    {
    //        InvokeRepeating("ReducePlayerEnergy", 0f, periodReducePlayerEnergy);
    //    }
    //    else
    //    {
    //        CancelInvoke("ReducePlayerEnergy");
    //    }
    //}

    public void UpdatePlayerEnergy()
    {
        playerEnergy += bonusEnergy;
        UIManager.Instance.textEnergy.text = playerEnergy.ToString();
    }

    public void ReducePlayerEnergy()
    {
        playerEnergy -= amountEnergyReduced;
        if (playerEnergy <= 0f)
        {
            playerEnergy = 0f;
            LeanTween.delayedCall(1f, () =>
            {
                SceneManager.LoadScene("Hung_Scene");
            });
            //LeanTween.delayedCall(0.05f, () =>
            //{
            //    //BackToMainMenu();
            //    //playerEnergy = originEnergy;
            //    //SaveLoadGameData.Instance.RefreshGameData();

            //});

        }
        UIManager.Instance.textEnergy.text = playerEnergy.ToString();
        //UIManager.Instance.UpdatePlayerEnergyBar(playerEnergy);
        UpdateGameData();
    }

    public void OnClickNewGame()
    {
        SaveLoadGameData.Instance.RefreshGameData();
        LoadInHomeScreen();
    }

    public void OnClickLoadGame()
    {
        LoadInHomeScreen();
    }

    private void LoadInHomeScreen()
    {
        UIManager.Instance.panelFading.StartFadePanel(() =>
        {
            inHomeCamera.gameObject.SetActive(true);
            mainCamera.gameObject.SetActive(false);
            UIManager.Instance.SetPanelMainMenu(false);
        },
        () =>
        {
            StoryLineManager.Instance.StartTimeLine();
            isPlaying = true;
            SoundManager.Instance.PlayBackgroundMusic(inHomeCamera.GetComponent<AudioSource>(), SoundManager.Instance.backgroundMusic, SoundManager.Instance.GlobalVolume, true);
        });
    }

    public void LoadMiniGameScreen()
    {
        UIManager.Instance.panelFading.StartFadePanel(() =>
        {
            SoundManager.Instance.StopBackgroundMusic(inHomeCamera.GetComponent<AudioSource>());
            miniGameCamera.gameObject.SetActive(true);
            inHomeCamera.gameObject.SetActive(false);
        },
        () =>
        {
            isPlaying = true;
            SoundManager.Instance.PlayBackgroundMusic(miniGameCamera.GetComponent<AudioSource>(), SoundManager.Instance.backgroundMusic, SoundManager.Instance.GlobalVolume, true);
        });
    }

    public void LoadHomeScreenFromMiniGame()
    {
        UIManager.Instance.panelFading.StartFadePanel(() =>
        {
            SoundManager.Instance.StopBackgroundMusic(inHomeCamera.GetComponent<AudioSource>());
            inHomeCamera.gameObject.SetActive(true);
            miniGameCamera.gameObject.SetActive(false);
        },
        () =>
        {
            isPlaying = true;
            SoundManager.Instance.PlayBackgroundMusic(inHomeCamera.GetComponent<AudioSource>(), SoundManager.Instance.backgroundMusic, SoundManager.Instance.GlobalVolume, true);
        });
    }

    public void BackToMainMenu()
    {
        if (inHomeCamera.gameObject.activeInHierarchy)
        {
            UIManager.Instance.panelFading.StartFadePanel(() =>
            {
                SoundManager.Instance.StopBackgroundMusic(inHomeCamera.GetComponent<AudioSource>());
                mainCamera.gameObject.SetActive(true);
                inHomeCamera.gameObject.SetActive(false);
                miniGameCamera.gameObject.SetActive(false);
                UIManager.Instance.SetPanelMainMenu(true);
            },
            () =>
            {
                isPlaying = false;
            });
            return;
        }

        if (miniGameCamera.gameObject.activeInHierarchy)
        {
            UIManager.Instance.panelFading.StartFadePanel(() =>
            {
                SoundManager.Instance.StopBackgroundMusic(miniGameCamera.GetComponent<AudioSource>());
                mainCamera.gameObject.SetActive(true);
                miniGameCamera.gameObject.SetActive(false);
                UIManager.Instance.SetPanelMainMenu(true);
            },
            () =>
            {
                isPlaying = false;
            });
            return;
        }
    }
}
