﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : Singleton<SoundManager>
{
    private float globalVolume;
    public float GlobalVolume
    {
        get
        {
            return globalVolume;
        }
        set
        {
            if (UIManager.Instance != null)
            {
                globalVolume = UIManager.Instance.slider.value;
            }
        }
    }

    [Header("Audio Clips")]
    public AudioClip audioButtonClick;
    public AudioClip backgroundMusic;

    private List<AudioSource> audioSourcesMusic = new List<AudioSource>();

    public void PlaySFX(AudioSource source, AudioClip clip, float volume)
    {
        source.volume = volume;
        source.loop = false;
        source.PlayOneShot(clip);
    }

    public void PlayBackgroundMusic(AudioSource source, AudioClip clip, float volume, bool loop = false)
    {
        source.clip = clip;
        source.loop = loop;
        source.volume = volume;
        source.Play();
        if (!audioSourcesMusic.Contains(source))
        {
            audioSourcesMusic.Add(source);
        }
    }

    public void StopBackgroundMusic(AudioSource source)
    {
        source.Stop();
        if (audioSourcesMusic.Contains(source))
        {
            audioSourcesMusic.Remove(source);
        }
    }

    public void PauseBackGroundMusic(AudioSource source)
    {
        source.Pause();
    }

    public void ResumeBackgroundMusic(AudioSource source)
    {
        source.UnPause();
    }

    public void UpdateVolume()
    {
        for (int i = 0; i < audioSourcesMusic.Count; i++)
        {
            audioSourcesMusic[i].volume = globalVolume;
        }
    }
}
