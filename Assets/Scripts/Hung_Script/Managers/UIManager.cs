﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIManager : Singleton<UIManager>
{
    private const string SETTING_SOUND = "Setting_Sound";

    public ProgressBar gameProgress;
    public ProgressBar playerEnergy;

    [Header("Menu")]
    public GameObject panelMainMenu;
    public GameObject panelSetting;
    public GameObject panelCredit;

    [Header("Setting")]
    public Slider slider;

    [Header("Fading Screen")]
    public PanelFading panelFading;

    [Header("Audio Source")]
    public AudioSource audioSource;

    [Header("Text energy in minigame")]
    public Text textEnergy;

    [Header("Win game")]
    public GameObject panelWinGame;


    private void Start()
    {
        Initialize();
    }

    private void Initialize()
    {
        LoadSettingSound();
        audioSource.playOnAwake = false;
        audioSource.loop = false;
        slider.onValueChanged.AddListener((sliderValue) =>
        {
            SoundManager.Instance.UpdateVolume();
        });
    }

    public void UpdateGameProgressBar(float progress)
    {
        gameProgress.ValueProgress = progress;
    }

    public void UpdatePlayerEnergyBar(float energy)
    {
        float progress = energy / 100f;
        playerEnergy.ValueProgress = progress;
    }


    public void OnOpenPanelSetting()
    {
        panelSetting.SetActive(true);
        SetPanelMainMenu(false);
    }

    public void OnClosePanelSetting()
    {
        panelSetting.SetActive(false);
        SetPanelMainMenu(true);
    }

    public void OnOpenPanelCredit()
    {
        panelCredit.SetActive(true);
        SetPanelMainMenu(false);
    }

    public void OnClosePanelCredit()
    {
        panelCredit.SetActive(false);
        SetPanelMainMenu(true);
    }

    public void SetPanelMainMenu(bool activte)
    {
        panelMainMenu.SetActive(activte);
    }

    private void LoadSettingSound()
    {
        if (PlayerPrefs.HasKey(SETTING_SOUND))
        {
            slider.value = PlayerPrefs.GetFloat(SETTING_SOUND);
            if (SoundManager.Instance != null)
            {
                SoundManager.Instance.GlobalVolume = slider.value;
            }
        }
        else
        {
            slider.value = 1f;
            SaveSoundSetting();
        }
    }

    public void SaveSoundSetting()
    {
        PlayerPrefs.SetFloat(SETTING_SOUND, slider.value);
        SoundManager.Instance.GlobalVolume = slider.value;
    }
}
