﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class DialogueManager : MonoBehaviour
{
	public delegate void FinishDialog();
	public static event FinishDialog OnFinishDialog;

	private static DialogueManager _instance;

	public static DialogueManager Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new DialogueManager();
			}

			return _instance;
		}
	}
	public bool isWish;
	public bool isPlaying;
	public bool isTyping;
	public TextMeshPro dialogueBasicText;
	public TextMeshPro dialogueWishText;
	public PopUpBaseManager popupManager;
	public float timeDelayType;
	public float timeShowPopup;
	private Queue<string> sentences;
	private void Awake()
	{
		_instance = this;
	}
	void Start()
	{
		sentences = new Queue<string>();
	}
	private void Update()
	{
		if (!isPlaying)
		{
			return;
		}
        
		if (Input.GetMouseButtonDown(0))
		{
			if (!isWish)
			{
				DisplayNextSentence();
			}
			else
			{
				DisplayWish();
			}
		}
	}
	public void StartDialogue(Dialogue dialogue)
	{
		isPlaying = true;
		isWish = false;
		//reset dialogue text
		dialogueBasicText.text = "";

		popupManager.ShowDialogue(timeShowPopup);
		sentences.Clear();

		foreach (string sentence in dialogue.sentences)
		{
			sentences.Enqueue(sentence);
		}

		DisplayNextSentence();
	}
	public void StartDialogueWish(Dialogue dialogue)
	{
		isPlaying = true;
		isWish = true;
		//reset dialogue text
		dialogueWishText.text = "";

		popupManager.ShowWishPopup(timeShowPopup);
		sentences.Clear();

		foreach (string sentence in dialogue.wishLines)
		{
			sentences.Enqueue(sentence);
		}

		DisplayWish();
	}
	public void DisplayNextSentence()
	{
		if (sentences.Count == 0)
		{
			EndDialogue();
			return;
		}

		string sentence = sentences.Dequeue();
		StopAllCoroutines();
		StartCoroutine(TypeSentence(sentence));
	}
	public void DisplayWish()
	{
		if (sentences.Count == 0)
		{
			EndWish();
			return;
		}

		string sentence = sentences.Dequeue();
		StopAllCoroutines();
		StartCoroutine(TypeWish(sentence));
	}
	IEnumerator TypeSentence(string sentence)
	{
		isTyping = true;
		yield return new WaitForSeconds(timeDelayType);
		dialogueBasicText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueBasicText.text += letter;
			yield return null;
		}
		isTyping = false;
	}
	IEnumerator TypeWish(string sentence)
	{
		isTyping = true;
		yield return new WaitForSeconds(timeDelayType);
		dialogueWishText.text = "";
		foreach (char letter in sentence.ToCharArray())
		{
			dialogueWishText.text += letter;
			yield return null;
		}
		isTyping = false;
	}

	void EndDialogue()
	{
		isPlaying = false;
		popupManager.CloseDialogue(timeShowPopup);
		if (OnFinishDialog != null)
		{
			OnFinishDialog();
			OnFinishDialog = null;
		}
	}
	void EndWish()
	{
		isPlaying = false;
		popupManager.CloseWishPopup(timeShowPopup);
		if (OnFinishDialog != null)
		{
			OnFinishDialog();
			OnFinishDialog = null;
		}
	}

}