﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceHolderController : MonoBehaviour
{
    public GameObject holder;
    private Vector3 playerInputOrigin;
    public bool isActive;

    [SerializeField, Range(0f, 100f)]
    float maxSpeed = 10f;

    [SerializeField, Range(0f, 100f)]
    float maxAcceleration = 10f;

    [SerializeField, Range(0f, 1f)]
    float bounciness = 0.5f;

    [SerializeField]
    Rect allowedArea = new Rect(-5f, -5f, 10f, 10f);

    Vector2 velocity;

    [SerializeField] Camera miniGameCamera;

    void Update()
    {
        if (!isActive)
        {
            return;
        }
        if (!miniGameCamera.enabled)
        {
            return;
        }
        if (Input.GetMouseButtonDown(0))
        {
            playerInputOrigin = Input.mousePosition;
            return;
        }
        if (!Input.GetMouseButton(0)) return;

        Vector2 playerInput = miniGameCamera.ScreenToViewportPoint(Input.mousePosition - playerInputOrigin);

        playerInput = Vector2.ClampMagnitude(playerInput, 1f);

        Vector2 desiredVelocity =
            new Vector2(playerInput.x, 0f) * maxSpeed;

        float maxSpeedChange = maxAcceleration * Time.deltaTime;
        velocity.x =
            Mathf.MoveTowards(velocity.x, desiredVelocity.x, maxSpeedChange);
        //Debug.Log(velocity.x);

        Vector3 displacement = velocity * Time.deltaTime;
        Vector3 newPosition = transform.localPosition + displacement;
        if (newPosition.x < allowedArea.xMin)
        {
            newPosition.x = allowedArea.xMin;
            velocity.x = -velocity.x * bounciness;
        }
        else if (newPosition.x > allowedArea.xMax)
        {
            newPosition.x = allowedArea.xMax;
            velocity.x = -velocity.x * bounciness;
        }
        transform.localPosition = newPosition;
        holder.transform.localPosition = newPosition;
    }



}
