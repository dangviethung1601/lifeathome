﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GamePlayManager : MonoBehaviour
{
    public delegate void EventGamepley();
    public static event EventGamepley Oncomplete;
    private static GamePlayManager _instance;

    public static GamePlayManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new GamePlayManager();
            }

            return _instance;
        }
    }
    [SerializeField]
    private GameObject homePos;
    [SerializeField]
    private GameObject gameplayPos;
    [SerializeField]
    private GameObject camera;

    [SerializeField]
    private PlaceHolderController catcher;
    [SerializeField]
    private GameObject[] listItem;
    [SerializeField]
    private Sprite[] listIcon;
    [SerializeField]
    private SpriteRenderer iconHolder;
    [SerializeField]
    private Transform spawningPos;
    [SerializeField]
    private float timeReSpawn;
    [SerializeField]
    private float timeShowPopup;
    [SerializeField]
    private float timeHoldPopup;
    [SerializeField]
    private float EvaluateValue = 0.3f;
    private float timer;
    private int currentPieceIndex;
    public int currentItemIndex;
    private bool isActive;
    private List<GameObject> pieceList;
    private void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        //currentItemIndex = -1;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            //currentItemIndex=0;
            ActiveDrop();
        }
        if (!isActive)
        {
            return;
        }
        if (currentPieceIndex >= pieceList.Count)
        {

            return;
        }
        if (timer <= timeReSpawn)
        {
            timer += Time.deltaTime;
        }
        else
        {
            timer = 0;
            GameObject piece = Instantiate(pieceList[currentPieceIndex], spawningPos.position, Quaternion.identity);
            currentPieceIndex++;
            if (piece.GetComponent<PieceController>())
            {
                piece.GetComponent<PieceController>().ActivePiece();
            }
        }
    }
    public void ActiveDrop()
    {
        //Set Selected item
        //listItem[currentItemIndex] = item;
        iconHolder.sprite = listIcon[currentItemIndex];
        //Get all piece
        pieceList = listItem[currentItemIndex].GetComponent<ItemController>().listPiece;

        currentPieceIndex = 0;

        isActive = true;

        //Assign MaxNumberPiece
        PieceCollector.Instance.MaxNumberPiece = pieceList.Count;

    }
    public void OnEndDropPiece(List<GameObject> pieceResultList)
    {
        //compare position 
        EvalueateResult(pieceResultList);
    }
    private bool EvalueateResult(List<GameObject> pieceResultList)
    {
        if (pieceResultList.Count != pieceList.Count)
        {
            Debug.Log("NotComplete");
            return false;
        }
        for (int i = 0; i < pieceResultList.Count; i++)
        {
            Debug.Log("Piece temp : " + pieceList[i].transform.localPosition + " piece result : " + pieceResultList[i].transform.localPosition);
            float distanceX = Mathf.Abs(pieceResultList[i].transform.localPosition.x - pieceList[i].transform.localPosition.x);
            float distanceY = Mathf.Abs(pieceResultList[i].transform.localPosition.y - pieceList[i].transform.localPosition.y);
            Debug.Log("distanceX : " + distanceX + "distanceY : " + distanceY);
            if (distanceX > EvaluateValue || distanceY > EvaluateValue)
            {
                StartCoroutine(OnFailed());
                Debug.Log("NotComplete");
                return false;
            }
        }
        StartCoroutine(OnCompleteItem());
        Debug.Log("Complete");
        return true;
    }
    IEnumerator OnFailed()
    {
        GameManager.Instance.ReducePlayerEnergy();
        yield return new WaitForSeconds(0.5f);
        ResetHolder();
        PopUpManager.Instance.OnFailed(timeShowPopup, timeHoldPopup);
        yield return new WaitForSeconds(timeHoldPopup + timeShowPopup);
        PopUpManager.Instance.OnReCreate(1, 1);
        yield return new WaitForSeconds(1 + 1);
        ActiveDrop();
    }
    IEnumerator OnCompleteItem()
    {
        GameManager.Instance.UpdatePlayerEnergy();
        yield return new WaitForSeconds(0.5f);
        //Update current item
        currentItemIndex++;
        ResetHolder();
        PopUpManager.Instance.OnComplete(timeShowPopup, timeHoldPopup);
        yield return new WaitForSeconds(timeHoldPopup + timeShowPopup);
        //Chuyen camera sang base gameplay
        //camera.transform.position = homePos.transform.position;
        GameManager.Instance.LoadHomeScreenFromMiniGame();
        yield return new WaitForSeconds(2);
        catcher.isActive = false;
        if (Oncomplete != null)
        {
            Oncomplete();
            Oncomplete = null;
        }
    }

    private void ResetHolder()
    {
        PieceCollector.Instance.ClearAllPiece();
    }
    public void StartPuzzle()
    {
        GameManager.Instance.LoadMiniGameScreen();

        catcher.isActive = true;
        //camera.transform.position = gameplayPos.transform.position;
        ActiveDrop();
    }

    //public bool IsEndGame()
    //{
    //    return currentItemIndex == listItem.Length - 1;
    //}
}
