﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpManager : MonoBehaviour
{
    private static PopUpManager _instance;

    public static PopUpManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PopUpManager();
            }

            return _instance;
        }
    }
    public GameObject failedPopup;
    public GameObject completePopup;
    public GameObject ReCreate;
    public Transform popUpPosition;
    public Transform originalPosition;
    private void Awake()
    {
        _instance = this;
    }

    void Start()
    {
        InitPopup();
    }
    void InitPopup()
    {
        failedPopup.transform.position = originalPosition.position;
        completePopup.transform.position = originalPosition.position;
        ReCreate.transform.position = originalPosition.position;
    }
    public void OnFailed(float timeMoveToShowPos, float holdPopUpTime)
    {
        StartCoroutine(ShowPopupAction(failedPopup, timeMoveToShowPos, holdPopUpTime));
    }
    public void OnComplete(float timeMoveToShowPos, float holdPopUpTime)
    {
        StartCoroutine(ShowPopupAction(completePopup, timeMoveToShowPos, holdPopUpTime));
    }
    public void OnReCreate(float timeMoveToShowPos, float holdPopUpTime)
    {
        StartCoroutine(ShowPopupAction(ReCreate, timeMoveToShowPos, holdPopUpTime));
    }
    IEnumerator ShowPopupAction(GameObject popup, float timeMoveToShowPos, float holdPopUpTime)
    {
        LeanTween.move(popup, popUpPosition, timeMoveToShowPos).setEase(LeanTweenType.easeOutElastic);
        yield return new WaitForSeconds(holdPopUpTime);
        LeanTween.move(popup, originalPosition, timeMoveToShowPos).setEase(LeanTweenType.easeInCirc);
    }
    void Update()
    {
        
    }
}
