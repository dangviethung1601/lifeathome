﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpBaseManager : MonoBehaviour
{
    public GameObject dialguePopup;
    public GameObject wishPopup;

    public Transform popUpPosition;
    public Transform originalPosition;

    void Start()
    {
        InitPopup();
    }
    void InitPopup()
    {
        dialguePopup.transform.position = originalPosition.position;
        wishPopup.transform.position = originalPosition.position;
    }
    public void ShowDialogue(float timeMoveToShowPos)
    {
        StartCoroutine(ShowPopupAction(dialguePopup, timeMoveToShowPos));
    }
    public void ShowWishPopup(float timeMoveToShowPos)
    {
        StartCoroutine(ShowPopupAction(wishPopup, timeMoveToShowPos));
    }
    public void CloseDialogue(float timeMoveToShowPos)
    {
        ClosePopup(dialguePopup, timeMoveToShowPos);
    }
    public void CloseWishPopup(float timeMoveToShowPos)
    {
        ClosePopup(wishPopup, timeMoveToShowPos);
    }
    IEnumerator ShowPopupAction(GameObject popup, float timeMoveToShowPos)
    {
        LeanTween.move(popup, popUpPosition, timeMoveToShowPos).setEase(LeanTweenType.easeOutElastic);
        yield return null;
    }
    void ClosePopup(GameObject popup, float timeMoveToShowPos)
    {
        LeanTween.move(popup, originalPosition, timeMoveToShowPos).setEase(LeanTweenType.easeInCirc);
    }

}
