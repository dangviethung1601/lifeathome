﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryLineCameraController : MonoBehaviour
{
    public GameObject storyLineCamera;
    public Transform[] listRoomPos;
    public float timeToMoveNextRoom;
    public bool isMoving;
    void Start()
    {
        
    }


    void Update()
    {
        
    }
    public void MoveToRoom(int indexRoom)
    {
        if (indexRoom < 0 || indexRoom >= listRoomPos.Length)
        {
            return;
        }
        isMoving = true;
        LeanTween.moveY(storyLineCamera, listRoomPos[indexRoom].position.y, timeToMoveNextRoom).setEase(LeanTweenType.easeInQuad);
    }
}
