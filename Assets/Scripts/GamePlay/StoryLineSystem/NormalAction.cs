﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NormalAction : ActionStoryLine
{
    [System.Serializable]
    public class Anim
    {
        public string name;
        public float lenght;
    }
    public bool isPlayPuzzle;
    public bool isNotPlayCompleteAction;
    public DialogueTrigger dialogueTrigger;
    public Animator animator;
    public float timeDelayEnterPuzzle;
    public float delayEndAction;

    public Anim animationStart;
    public Anim animationComplete;
    void Start()
    {
        
    }

    void Update()
    {

    }
    public override void StartAction()
    {
        base.StartAction();
        StartCoroutine(PlayerAnimationStart());
    }
    public override void EndAction()
    {
        base.EndAction();
    }
    IEnumerator PlayerAnimationStart()
    {
        animator.Play(animationStart.name);
        yield return new WaitForSeconds(animationStart.lenght*2);
        PlayDialogue();
    }
    IEnumerator PlayerAnimationComplete()
    {
        animator.Play(animationComplete.name);
        yield return new WaitForSeconds(animationComplete.lenght*2);
        yield return new WaitForSeconds(delayEndAction);
        EndAction();

    }
    public void OnEndDialogue()
    {
        if (isNotPlayCompleteAction) {
            Invoke("EndAction", delayEndAction);
        }
        else if(!isNotPlayCompleteAction&&!isPlayPuzzle)
        {
            StartCoroutine(PlayerAnimationComplete());
        }
        else if(!isNotPlayCompleteAction && isPlayPuzzle)
        {
            Invoke("PlayWish",1f);
        }
    }
    public void PlayDialogue()
    {
        dialogueTrigger.TriggerDialogue();
        DialogueManager.OnFinishDialog += OnEndDialogue;
    }
    public void PlayWish()
    {
        dialogueTrigger.PlayWish();
        DialogueManager.OnFinishDialog += StartPuzzleDelay;
        
        //Start Puzzle On En Wish
    }
    public void StartPuzzleDelay()
    {
        Invoke("StartPuzzle", timeDelayEnterPuzzle);
    }
    public void StartPuzzle()
    {
        GamePlayManager.Instance.StartPuzzle();
        GamePlayManager.Oncomplete += OnCompletePuzzle;
    }
    public void OnCompletePuzzle()
    {
        StartCoroutine(PlayerAnimationComplete());
    }
}
