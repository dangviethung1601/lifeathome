﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryLineManager : MonoBehaviour
{
    private static StoryLineManager _instance;

    public static StoryLineManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new StoryLineManager();
            }

            return _instance;
        }
    }
    public StoryLineCameraController cameraStory;
    public int currentIndexRoom;
    public int currentIndexAction;
    public ActionStoryLine currentAction;
    public ActionStoryLine[] timeLine;
    public int[] listMaxItemNumber;
    private int LimitTop;
    private void Awake()
    {
        _instance = this;
    }

    void Update()
    {

    }
    void OnNextRoom()
    {
        currentIndexRoom++;
        cameraStory.MoveToRoom(currentIndexRoom);
    }
    public void OnNextAction()
    {
        if (currentIndexAction + 1 >= timeLine.Length)
        {
            Debug.Log("Comeplete All Action");
            //Call Win Action
            GameManager.Instance.Win();
            //Load Menu
            return;
        }
        //CheckIfChuyenTang
        int maxItem = 0;
        for (int i = 0; i <= currentIndexRoom; i++)
        {
            maxItem += listMaxItemNumber[i];
        }
        if (currentIndexAction >= maxItem - 1)
        {
            StartCoroutine(NextRoomAction());
            return;
        }
        timeLine[currentIndexAction].PlayNextAction(timeLine[currentIndexAction + 1]);

        currentIndexAction++;

        currentAction = timeLine[currentIndexAction];
    }
    IEnumerator NextRoomAction()
    {
        //Move Camera;

        yield return new WaitForSeconds(0.5F);
        OnNextRoom();
        yield return new WaitForSeconds(2);
        timeLine[currentIndexAction].PlayNextAction(timeLine[currentIndexAction + 1]);

        currentIndexAction++;

        currentAction = timeLine[currentIndexAction];
    }
    public void StartTimeLine()
    {
        currentIndexRoom = 0;
        currentIndexAction = 0;
        timeLine[currentIndexAction].StartAction();
    }
}
