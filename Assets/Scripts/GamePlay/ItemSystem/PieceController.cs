﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceController : MonoBehaviour
{
    
    public  Rigidbody2D rigidbody;
    public  float timeReadyDrop;

    private bool isAssigned;
    void Start()
    {
        rigidbody.isKinematic = true;
    }

    void Update()
    {
        
    }
    public void SetParent(GameObject holder)
    {
        transform.parent = holder.transform;
    }
   
    public void ActivePiece()
    {
        LeanTween.alpha(gameObject, 1, timeReadyDrop).setOnComplete(() =>
         {
             ActivePhysic();
         });
    }
    private void ActivePhysic()
    {
        rigidbody.isKinematic = false;
    }
    private void OffPhysic()
    {
        rigidbody.isKinematic = true;
        rigidbody.velocity = Vector2.zero;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (isAssigned)
        {
            return;
        }
        isAssigned = true;
        OffPhysic();
        PieceCollector.Instance.AssignPiece(gameObject);
    }
}
