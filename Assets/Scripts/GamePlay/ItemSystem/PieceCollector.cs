﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PieceCollector : MonoBehaviour
{

    private static PieceCollector _instance;

    public static PieceCollector Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new PieceCollector();
            }

            return _instance;
        }
    }
    public GameObject holderAllPiece;
    public GameObject slider;
    public int MaxNumberPiece = 0;
    public List<GameObject> listPiece;
    private void Awake()
    {
        _instance = this;
    }
    void Start()
    {
        listPiece = new List<GameObject>();
    }

    void Update()
    {
        
    }
   
    public void AssignPiece(GameObject piece)
    {
        if (listPiece.Count == 0)
        {
            holderAllPiece.transform.position = piece.transform.position;
        }
        piece.transform.parent = holderAllPiece.transform;
        listPiece.Add(piece);
        if (listPiece.Count == MaxNumberPiece)
        {
            Debug.Log("count Piece : " + listPiece.Count);
            //OnEndRound
            GamePlayManager.Instance.OnEndDropPiece(listPiece);
        }
    }
    public void ClearAllPiece()
    {
        MaxNumberPiece = 0;
        ClearHolder();
    }
    public void ClearHolder()
    {
        int lenght = listPiece.Count;
        for (int i = 0; i < lenght; i++)
        {
            GameObject piece = listPiece[0];
            listPiece.Remove(piece);
            Destroy(piece);
        }
    }
}
